#ifndef _ARGS_H_
#define _ARGS_H_

#include "config.h"

void usage(void);
char** args_copy(int argc, char *argv[]);
struct Config* args_parse(int, char*[]);

#endif

