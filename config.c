/*
	The MIT License (MIT)
	Copyright (c) 2016 Christian Fiedler

	Permission is hereby granted, free of charge, to any person
	obtaining a copy of this software and associated documentation
	files (the "Software"), to deal in the Software without
	restriction, including without limitation the rights to use,
	copy, modify, merge, publish, distribute, sublicense,
	and/or sell copies of the Software, and to permit persons to
	whom the Software is furnished to do so, subject to the
	following conditions:

	The above copyright notice and this permission notice shall be
	included in all copies or substantial portions of the Software.

	THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
	EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
	OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
	NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
	HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
	WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
	FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
	OTHER DEALINGS IN THE SOFTWARE.
*/

#include "telnetd.h"
#include "config.h"

#include <assert.h>
#include <stdlib.h>
#include <string.h>
#include <sys/mman.h>
#include <unistd.h>

void config_init(struct Config *const conf)
{
	conf->program = NULL;
	conf->log_path = NULL;
	conf->chroot_path = NULL;
	conf->port = NULL;
	conf->flags = 0;
	conf->unpriv_user = NULL;
	conf->unpriv_group = NULL;
	conf->unpriv_uid = 0;
	conf->unpriv_gid = 0;
}

struct Config* config_sec_alloc(void)
{
	void* conf;
	int pgsize = getpagesize();

	if(posix_memalign(&conf, pgsize, sizeof(struct Config)) != 0){
		return NULL;
	}
	assert(pgsize >= sizeof(struct Config));

	return (struct Config*) conf;
}

void config_sec_finish(struct Config* conf)
{
	mprotect(conf, sizeof(struct Config), PROT_READ);
}

uid_t config_parse_user(const char *s)
{
	uid_t uid;
	char *endptr;
	long tmp = strtol(s, &endptr, 10);

	if(*endptr == '\0'){
		if(tmp < 0){
			die("invalid user id");
		}

		uid = tmp;
	}else{
		struct passwd* pw = getpwnam(s);
		if(pw == NULL){
			die("invalid username");
		}

		uid = pw->pw_uid;
	}

	return uid;
}

gid_t config_parse_group(const char *s)
{
	gid_t gid;
	char *endptr;
	long tmp = strtol(s, &endptr, 10);

	if(*endptr == '\0'){
		if(tmp < 0){
			die("invalid group id");
		}

		gid = tmp;
	}else{
		struct group* gr = getgrnam(s);
		if(gr == NULL){
			die("invalid group name");
		}

		gid = gr->gr_gid;
	}

	return gid;
}

int config_check_bool(const char *s)
{
	if(strcmp(s,"yes") == 0 || strcmp(s,"true") == 0 ||
		strcmp(s,"1") == 0 || strcmp(s,"on") == 0){

		return 1;
	}else if(strcmp(s,"no") == 0 || strcmp(s,"false") == 0||
		strcmp(s,"0") == 0 || strcmp(s,"off") == 0){

		return 0;
	}else{
		return -1;
	}
}

void config_set_unpriv_ug(struct Config *const conf)
{
	if(conf->unpriv_uid == 0 && conf->unpriv_user != NULL){
		conf->unpriv_uid = config_parse_user(conf->unpriv_user);
	}

	if(conf->unpriv_gid == 0 && conf->unpriv_group != NULL){
		conf->unpriv_gid = config_parse_group(conf->unpriv_group);

		/* if uid was set and gid was not -> use user's primary
		   group as gid */
		if(conf->unpriv_gid == 0 && conf->unpriv_user != NULL){
			struct passwd *pw = getpwnam(conf->unpriv_user);
			conf->unpriv_gid = pw->pw_gid;
		}
	}
}

int config_check_struct(const struct Config *const conf, const char **errmsg)
{
	int status = 0; /* 0 = ok, 1 = warning, 2 = error */
	if(errmsg != NULL) *errmsg = NULL;

	if(conf->port == NULL){
		if(errmsg != NULL) *errmsg = "no port specified\n";
		return 2;
	}

	if((conf->flags & CONF_NO_CHROOT) == 0 && conf->chroot_path == NULL){
		if(errmsg != NULL) *errmsg = "please specify a chroot directory or disable chroot!\n";
		return 2;
	}else if((conf->flags & CONF_NO_CHROOT) != 0 && conf->chroot_path != NULL){
		if(errmsg != NULL) *errmsg = "To chroot or not to chroot? What do you want?\n";
		return 2;
	}

	if(getuid() == 0 && conf->unpriv_uid == 0){
		if(errmsg != NULL) *errmsg = "telnetd will run as root with no effective privilege dropping!\n";
		status = 1;
	}

	if(getuid() != 0 && (conf->flags & CONF_NO_CHROOT) == 0){
		if(errmsg != NULL) *errmsg = "cannot chroot if not running as root!\n";
		return 2;
	}

	return status;
}

