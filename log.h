#ifndef LOG_H
#define LOG_H

#ifndef DISABLE_LOG

#define LOG(...)		log_write(__VA_ARGS__)
#define LOG_INIT()		log_init()
#define LOG_SETPATH(P)		log_setpath(P)
#define LOG_SETSTDERR()		log_setstderr()
#define LOG_CLOSE()		log_close()

#define LOG_LINEMAX		150

/* returns 0 on failure */
int log_init(void);

/* returns 0 on failure */
int log_setpath(const char*);

/* returns 0 on failure */
void log_setstderr(void);

void log_write(const char*, ...);

void log_close(void);

#else


#define LOG(...)		printf(__VA_ARGS__)
#define LOG_INIT()		1
#define LOG_SETPATH(P)		1
#define LOG_SETSTDERR()
#define LOG_CLOSE()


#endif /* DISABLE_LOG */


#endif /* LOG_H - INCLUDE GUARD */

