#ifndef _TELNETD_H_
#define _TELNETD_H_

#ifdef __linux__
#define _POSIX_SOURCE
#define _XOPEN_SOURCE 500
#else
#define _BSD_SOURCE
#endif

#ifndef NDEBUG
#define debug(...)	fprintf(stderr, __VA_ARGS__);
#else
#define	debug(...)
#endif


/* DEFINES */
#define DEFAULT_LOG_PATH	"/var/log/telnetd.log"
#define BACKLOG_NUM	50

/* MACROS */
#define PORT_OK(P)	((P) >= 1 && (P) <= 65535)
#define ARRAYLEN(A)	(sizeof(A)/sizeof(*(A)))

extern const char *progname;
extern const char ERR_OUT_OF_MEMORY[];

void die(const char*);
void die_assert(const char*,int);
void warn(const char*);

#endif

