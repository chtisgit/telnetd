/*
	The MIT License (MIT)
	Copyright (c) 2016 Christian Fiedler

	Permission is hereby granted, free of charge, to any person
	obtaining a copy of this software and associated documentation
	files (the "Software"), to deal in the Software without
	restriction, including without limitation the rights to use,
	copy, modify, merge, publish, distribute, sublicense,
	and/or sell copies of the Software, and to permit persons to
	whom the Software is furnished to do so, subject to the
	following conditions:

	The above copyright notice and this permission notice shall be
	included in all copies or substantial portions of the Software.

	THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
	EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
	OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
	NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
	HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
	WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
	FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
	OTHER DEALINGS IN THE SOFTWARE.
*/

#include "telnetd.h"

#include <assert.h>
#include <errno.h>
#include <signal.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <poll.h>
#include <unistd.h>
#include <fcntl.h>

#include <arpa/inet.h>
#include <netinet/in.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <sys/wait.h>

#include "log.h"
#include "args.h"

const char *progname;
const char ERR_OUT_OF_MEMORY[] = "out of memory! terminating.";

void die(const char* s)
{
	fprintf(stderr, "error: %s\n\n", s);
	exit(EXIT_FAILURE);
}
void die_assert(const char* s, int x)
{
	if(!x) die(s);
}

void warn(const char* s)
{
	fprintf(stderr, "warning: %s\n", s);
}

static int init_server_socket(const char* sport)
{
	int sock = socket(AF_INET, SOCK_STREAM, 0);
	const int port = atoi(sport);
	
	if(! PORT_OK(port)){
		die("port out of range");
	}

	if(sock == -1){
		die("socket() failed");
	}

	int enable = 1;
	if(setsockopt(sock, SOL_SOCKET, SO_REUSEADDR, &enable, sizeof(int)) != 0){
		die("setsockopt() failed");
	}
	
	struct sockaddr_in addr = {
		.sin_family = AF_INET,
		.sin_port = htons(port),
		.sin_addr = { .s_addr = INADDR_ANY }
	};

	if(bind(sock, (struct sockaddr*) &addr, sizeof(addr)) != 0){
		die("bind() failed");
	}

	if(listen(sock, BACKLOG_NUM) != 0){
		die("listen() failed");
	}

	return sock;
}

struct Data{
	int sock;
	struct sockaddr addr;
	char **argv;
	const struct Config *conf;
};

static int pipe_to_socket(int pipefd, int sockfd)
{
	ssize_t len;
	char c[64];

	len = read(pipefd, &c, sizeof(c));

	if(len > 0){
		send(sockfd, &c, len, MSG_NOSIGNAL);
	}

	return len != 0;
}
static int socket_to_pipe(int sockfd, int pipefd)
{
	char c[64];
	ssize_t len;

	for(;;){
		errno = 0;
		len = recv(sockfd, &c, sizeof(c), MSG_DONTWAIT);

		if(len == 0 || errno == EAGAIN || errno == EWOULDBLOCK){
			break;
		}

		if(len > 0){
			write(pipefd, &c, len);
		}
	}

	return len != 0;
}

static void drop_privileges(const struct Config* conf)
{
	if(getuid() == 0){
		if((conf->flags & CONF_NO_CHROOT) == 0){
			LOG("chroot to %s\n", conf->chroot_path);
			if(chroot(conf->chroot_path) != 0){
				die("chroot failed");
			}
		}else{
			LOG("NO CHROOT!");
		}
		if(conf->unpriv_gid > 0 && conf->unpriv_uid > 0){
			int r, tries = 10;
			while(tries > 0){
				errno = 0;
				r = setregid(conf->unpriv_gid, conf->unpriv_gid);
				if(r == 0) break; /* ok */
				if(errno == EPERM || errno == EINVAL){
					/* non-temporary failures */
					die("setregid failed permanently");
				}
				sleep(2);
				tries--;
			}
			if(tries == 0){
				die("maximum number of tries to setregid/setreuid exceeded");
			}
			while(tries > 0){
				errno = 0;
				r = setreuid(conf->unpriv_uid, conf->unpriv_uid);
				if(r == 0) break; /* ok */
				if(errno == EPERM || errno == EINVAL){
					/* non-temporary failures */
					die("setreuid failed permanently");
				}
				sleep(2);
				tries--;
			}
			if(tries == 0){
				die("maximum number of tries to setregid/setreuid exceeded");
			}

			assert(getgid() != 0);
			assert(getuid() != 0);
		}
	}
	LOG("running with uid=%d gid=%d\n",getuid(),getgid());
}

static void manager_process(struct Data *d)
{
	int prog_stderr[2];
	int prog_stdout[2];
	int prog_stdin[2];
	int run;

	if(pipe(prog_stderr) != 0){
		die("pipe() failed");
	}
	if(pipe(prog_stdout) != 0){
		die("pipe() failed");
	}
	if(pipe(prog_stdin) != 0){
		die("pipe() failed");
	}

	pid_t pid = fork();

	if(pid == -1){
		die("fork() error");
	}

	if(pid == 0){
		/* CHILD */
		close(prog_stderr[0]);
		close(prog_stdout[0]);
		close(prog_stdin[1]);
		
		if(dup2(prog_stderr[1], STDERR_FILENO) == -1){
			exit(EXIT_FAILURE);
		}
		close(prog_stderr[1]);
		
		if(dup2(prog_stdout[1], STDOUT_FILENO) == -1){
			exit(EXIT_FAILURE);
		}
		close(prog_stdout[1]);

		if(dup2(prog_stdin[0], STDIN_FILENO) == -1){
			exit(EXIT_FAILURE);
		}
		close(prog_stdin[0]);

		execvp(d->argv[0], d->argv);
		exit(EXIT_FAILURE);
	}else{
		/* PARENT */
		int i;

		close(prog_stderr[1]);
		close(prog_stdout[1]);
		close(prog_stdin[0]);

		struct pollfd pfds[] = {
			{ .fd = prog_stderr[0], .events = POLLIN | POLLPRI },
			{ .fd = prog_stdout[0], .events = POLLIN | POLLPRI },
			{ .fd = prog_stdin[1], .events = POLLHUP | POLLERR },
			{ .fd = d->sock, .events = POLLIN | POLLPRI | POLLHUP | POLLERR },
		};
		enum{ /* keep in sync with pfds array */
			PFD_PROG_STDERR = 0,
			PFD_PROG_STDOUT = 1,
			PFD_SOCKET = 2,
			PFD_PROG_STDIN = 3
		};

		LOG("running ");
		for(i = 0; d->argv[i] != NULL; i++){
			LOG("%s ", d->argv[i]);
		}
		LOG("\n");

		run = 1;

		while(run){
			if(waitpid(pid, NULL, WNOHANG) == pid){
				run = 0;
				break;
			}

			pfds[PFD_PROG_STDERR].revents = 0;
			pfds[PFD_PROG_STDOUT].revents = 0;
			pfds[PFD_PROG_STDIN].revents = 0;
			pfds[PFD_SOCKET].revents = 0;
			const int count = poll(pfds, ARRAYLEN(pfds), 500);

			if(count > 0){
				if(pfds[PFD_SOCKET].revents & POLLHUP || pfds[PFD_SOCKET].revents & POLLERR ||
				   pfds[PFD_PROG_STDIN].revents & POLLHUP || pfds[PFD_PROG_STDIN].revents & POLLERR){
					run = 0;
				}else{
					if(pfds[PFD_PROG_STDERR].revents & POLLIN){
						run = pipe_to_socket(prog_stderr[0], d->sock);
					}
					if(pfds[PFD_PROG_STDOUT].revents & POLLIN){
						run = pipe_to_socket(prog_stdout[0], d->sock);
					}
					if(pfds[PFD_PROG_STDIN].revents & POLLIN){
						run = socket_to_pipe(d->sock, prog_stdin[1]);
					}
				}
				if(run == 0){
					kill(pid, SIGTERM);
				}
			}
		}
		
		close(prog_stdout[0]);
		close(prog_stdin[1]);
		
		close(d->sock);
		LOG("Connection closed.\n");
		
		free(d->argv);
		free(d);
	}
}

static sig_atomic_t terminated = 0;
static void sig_handler(int sig)
{
	terminated = 1;
}
static void sig_handler_setup(void)
{
	struct sigaction sa = {
		.sa_handler = sig_handler,
		.sa_flags = 0
	};
	sigfillset(&sa.sa_mask);

	sigaction(SIGINT, &sa, NULL);
	sigaction(SIGTERM, &sa, NULL);

	sa.sa_handler = SIG_DFL;
	sa.sa_flags = SA_NOCLDWAIT;
	sigaction(SIGCHLD, &sa, NULL);
}

int main(int argc, char *argv[])
{
	progname = argv[0];

	int processes;
	struct Data *child_data;
	struct Config *conf = args_parse(argc, argv);
	
	if(conf == NULL){
		usage();
		return EXIT_FAILURE;
	}

	LOG_INIT();
	if(LOG_SETPATH(conf->log_path == NULL ? DEFAULT_LOG_PATH : conf->log_path) == 0){
		if(conf->log_path != NULL){
			die("could not open log file!");
		}
		LOG_SETSTDERR();
	}

	int servsock = init_server_socket(conf->port);

	drop_privileges(conf);

	sig_handler_setup();

	processes = 0;
	while(terminated == 0){
		socklen_t sl;
		pid_t pid;

		child_data = malloc(sizeof(*child_data));
		die_assert(ERR_OUT_OF_MEMORY, child_data != NULL);

		do{
			sl = sizeof(child_data->addr);

			errno = 0;
			child_data->sock = accept(servsock, &child_data->addr, &sl);
			if(errno == EINTR && terminated != 0){
				free(child_data);
				goto clean_exit;
			}
		}while(child_data->sock == -1);

		assert(conf->end_argc < argc);
		child_data->argv = args_copy(argc - conf->end_argc, argv + conf->end_argc);
		if(child_data->argv == NULL){
			LOG("could not copy args (low memory). Disconnecting client.\n");
			close(child_data->sock);
			free(child_data);
			continue;
		}

		pid = fork();

		if(pid == 0){
			/* CHILD */
			manager_process(child_data);
			exit(EXIT_SUCCESS);
		}else{
			/* PARENT */

			++processes;
			free(child_data->argv);

			char ip[INET_ADDRSTRLEN+1];
			const char *const err = inet_ntop(AF_INET, &child_data->addr, ip, sl);
			ip[INET_ADDRSTRLEN] = '\0';

			close(child_data->sock);
			free(child_data);

			if(pid == -1){
				LOG("non-fatal error: could not fork (peer was disconnected)\n");
			}else{
				if(err != NULL){
					LOG("accepted new connection from %s\n", ip);
				}else{
					LOG("accepted new connection\n");
				}
			}
		}
	}

clean_exit:
	LOG("Waiting for child processes to exit...\n");

	close(servsock);

	while(--processes >= 0){
		wait(NULL);
	}

	LOG("Terminating.\n");
	LOG_CLOSE();

	return EXIT_SUCCESS;
}

