#ifndef _CONFIG_H_
#define _CONFIG_H_

#include <unistd.h>

#include <pwd.h>
#include <grp.h>
#include <sys/types.h>

enum{
	CONF_NO_CHROOT = 0x0001,
};

struct Config{
	const char *program;	/* the server program */
	const char *log_path;	/* path for log file -
				   if NULL use default */
	const char *chroot_path;
	const char *port;	/* server port to listen on */
	const char *unpriv_user;
	const char *unpriv_group;
	unsigned flags;		/* if non-zero do not chroot */

	uid_t unpriv_uid;
	gid_t unpriv_gid;

	int end_argc;
};

void config_init(struct Config *const conf);
struct Config* config_sec_alloc(void);
void config_sec_finish(struct Config* conf);

uid_t config_parse_user(const char *s);
gid_t config_parse_group(const char *s);
int config_check_bool(const char *s);
void config_set_unpriv_ug(struct Config *const);
int config_check_struct(const struct Config *const, const char **);


#endif

