CFLAGS = -ansi -Wall -O2 -g
OBJS = telnetd.o log.o args.o config.o

all: telnetd
	
telnetd: $(OBJS)
	$(CC) -o $@ $(OBJS)

%.o:%.c
	$(CC) $(CFLAGS) -o $@  -c $<
	
clean:
	rm -f telnetd *.o

.PHONY: all clean
