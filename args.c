/*
	The MIT License (MIT)
	Copyright (c) 2016 Christian Fiedler

	Permission is hereby granted, free of charge, to any person
	obtaining a copy of this software and associated documentation
	files (the "Software"), to deal in the Software without
	restriction, including without limitation the rights to use,
	copy, modify, merge, publish, distribute, sublicense,
	and/or sell copies of the Software, and to permit persons to
	whom the Software is furnished to do so, subject to the
	following conditions:

	The above copyright notice and this permission notice shall be
	included in all copies or substantial portions of the Software.

	THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
	EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
	OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
	NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
	HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
	WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
	FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
	OTHER DEALINGS IN THE SOFTWARE.
*/

#include "telnetd.h"
#include "args.h"

#include <assert.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>

#define OPTSTR	"L:c:g:np:u:"

#define USAGE_STR	"USAGE: %s -p <port> [-n | -c chroot-path] [-L logfile] " \
			"[-u user(id)] [-g group(id)] " \
			"<server-program> [program-args ...]\n\n"

void usage(void)
{
	fprintf(stderr, USAGE_STR, progname);
}

char** args_copy(int argc, char *argv[])
{
	size_t i, len = 0;

	for(i = 0; i < argc; i++){
		len += strlen(argv[i])+1;
	}

	char **header = (char**) malloc((argc+1)*sizeof(char*) + len);
	char *text = (char*) (header + argc + 1);

	if(header == NULL)
		return NULL;

	len = 0;
	for(i = 0; i < argc; i++){
		strcpy(text + len, argv[i]);
		header[i] = text + len;
		len += strlen(argv[i])+1;
	}
	header[argc] = NULL;

	return header;
}

struct Config* args_parse(int argc, char **argv)
{
	int ch;
	struct Config conf, *confp;

	config_init(&conf);

	while((ch = getopt(argc, argv, OPTSTR)) != -1){
		switch(ch){
		case 'n':
			conf.flags |= CONF_NO_CHROOT;
			break;
		case 'c':
			conf.chroot_path = optarg;
			break;
		case 'u':
			conf.unpriv_user = optarg;
			break;
		case 'g':
			conf.unpriv_group = optarg;
			break;
		case 'L':
			conf.log_path = optarg;
			break;
		case 'p':
			conf.port = optarg;
			break;
		case '?':
		default:
			return NULL;
		}
	}
	if(optind >= argc){
		usage();
		die("no server program given!");
	}
	conf.end_argc = optind;

	config_set_unpriv_ug(&conf);

	const char *errmsg;
	int err = config_check_struct(&conf, &errmsg);

	if(err == 2) usage();
	die_assert(errmsg, err != 2);
	if(err == 1){
		warn(errmsg);
	}

	confp = config_sec_alloc();
	*confp = conf;
	config_sec_finish(confp);

	return confp;
}



