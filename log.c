/*
	The MIT License (MIT)
	Copyright (c) 2016 Christian Fiedler

	Permission is hereby granted, free of charge, to any person
	obtaining a copy of this software and associated documentation
	files (the "Software"), to deal in the Software without
	restriction, including without limitation the rights to use,
	copy, modify, merge, publish, distribute, sublicense,
	and/or sell copies of the Software, and to permit persons to
	whom the Software is furnished to do so, subject to the
	following conditions:

	The above copyright notice and this permission notice shall be
	included in all copies or substantial portions of the Software.

	THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
	EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
	OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
	NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
	HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
	WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
	FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
	OTHER DEALINGS IN THE SOFTWARE.
*/

#define _BSD_SOURCE

#include <assert.h>
#include <stdarg.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>

#include "log.h"

#ifndef DISABLE_LOG


static struct{
	const char *filename;
	FILE *file;
} log;

int log_init(void)
{
	log.file = NULL;
	log.filename = NULL;
	return 1;
}

void log_write(const char* fmt, ...)
{
	assert(log.file != NULL);
	va_list args;
	
	/* write time */
	time_t t = time(NULL);
	char *s = ctime(&t);
	s[24] = '\0'; /* clear \n */
	fprintf(log.file, "[%s] ", s);

	va_start(args, fmt);
	vfprintf(log.file, fmt, args);
	va_end(args);
}

int log_setpath(const char* logfile)
{
	assert(logfile != NULL);

	log.filename = strdup(logfile);
	if(log.filename == NULL){
		return 0;
	}
	log.file = fopen(logfile, "a");
	if(log.file == NULL){
		free((void*)log.filename);
		return 0;
	}

	log_write("Opened log file for writing.\n");

	return 1;
}
void log_setstderr(void)
{
	log.filename = "<stderr>";
	log.file = stderr;
}

void log_close(void)
{
	assert(log.filename != NULL && log.file != NULL);

	if(log.file != stderr){
		free((void*)log.filename);
		fclose(log.file);
	}
}

#endif

